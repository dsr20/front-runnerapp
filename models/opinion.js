var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var opinionSchema = new Schema({
  puntuacion:    { type: Number },
  nivel:  { type: Number },
  comentario:    { type: String },
  ruta:          { type: Schema.ObjectId, ref: "Rutas" }


});

module.exports = mongoose.model('Opinion', opinionSchema);
