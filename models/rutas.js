var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var rutasSchema = new Schema({
  nombre:    { type: String },
  dificultad:  { type: Number },
  popularidad:    { type: Number },
  descripcion:    { type: String },
  tipo:     { type: Number },
  usuario: { type:Number }

});

module.exports = mongoose.model('Rutas', rutasSchema);
