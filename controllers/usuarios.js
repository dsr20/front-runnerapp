//File: controllers/tvshows.js
var mongoose = require('mongoose');
var Usuario = require('../models/usuarios.js');
var express = require('express');

var router = express.Router();

//GET - Return all tvshows in the DB
router.get('/',function(req,res) {
	Usuario.find(function(err, usuario) {
		if(err) res.send(500, err.message);

	  console.log('GET /usuario')
		res.status(200).jsonp(usuario);
	});
});

//GET - Return a TVShow with specified ID
router.get('/:id',function(req, res) {
	Usuario.findById(req.params.id, function(err, usuario) {
    if(err) return res.send(500, err.message);

    console.log('GET /usuario/' + req.params.id);
		res.status(200).jsonp(usuario);
	});
});

//POST - Insert a new TVShow in the DB
router.post('/',function(req, res) {
	console.log('POST');
	console.log(req.body);

	var usuario = new Usuario({
		nombre:    req.body.nombre,
		password: 	  req.body.password,

	});

	usuario.save(function(err, usuario) {
		if(err) return res.send(500, err.message);
    res.status(200).jsonp(usuario);
	});
});

//PUT - Update a register already exists
router.put('/:id',function(req, res) {
	Usuario.findByIdAndUpdate(req.params.id,req.body,function(err, usuario) {
			if(err) return res.send(500, err.message);
      res.status(200).jsonp(usuario);
	});
});

//DELETE - Delete a TVShow with specified ID
router.delete('/:id',function(req, res) {
	console.log('DELETE');
	console.log(req.params.id);
	Usuario.findByIdAndRemove(req.params.id, function(err) {
		if(err) return res.send(500, err.message);
	  res.status(200).json({ message: 'Usuario elminado correctamente!' });
	});
});

module.exports = router;
