//File: controllers/tvshows.js
var mongoose = require('mongoose');
var Rutas = require('../models/rutas.js');
var express = require('express');

var router = express.Router();

//GET - Return all tvshows in the DB
router.get('/',function(req,res) {

	Rutas.find(function(err, rutas) {
		if(err) res.send(500, err.message);

	  console.log('GET /rutas')
		console.log(rutas)
		res.status(200).jsonp(rutas);
	});

});


router.get('/:id',function(req, res) {
	Rutas.findById(req.params.id, function(err, rutas) {
    if(err) return res.send(500, err.message);

    console.log('GET /rutas/' + req.params.id);
		res.status(200).jsonp(rutas);
	});
});

//POST - Insert a new TVShow in the DB
router.post('/',function(req, res) {
	console.log('POST');
	console.log(req.body);

	var rutas = new Rutas({
		nombre:    req.body.nombre,
	  dificultad:  req.body.dificultad,
	  popularidad:    req.body.popularidad,
	  descripcion:    req.body.descripcion,
	  tipo:     req.body.tipo,
	  usuario: req.body.usuario
	});

	rutas.save(function(err, rutas) {
		if(err) return res.send(500, err.message);
    res.status(200).jsonp(rutas);
	});
});

//DELETE - Delete a TVShow with specified ID
router.delete('/:id',function(req, res) {
	console.log('DELETE');
	console.log(req.params.id);
	Rutas.findByIdAndRemove(req.params.id, function(err) {
		if(err) return res.send(500, err.message);

	})

	Rutas.find(function(err, rutas) {
            if(err){
                res.send(err);
            }
            res.status(200).json(rutas);
        });
});



module.exports = router;
