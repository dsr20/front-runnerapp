//File: controllers/tvshows.js
var mongoose = require('mongoose');
var Opinion = require('../models/opinion.js');
var express = require('express');

var router = express.Router();

//GET - Return all tvshows in the DB
router.get('/',function(req,res) {

	Opinion.find(function(err, opinion) {
		if(err) res.send(500, err.message);

	  console.log('GET /opinion')
		res.status(200).jsonp(opinion);
	});

});

router.get('/rutas/:id',function(req, res){
	Opinion.find({ruta :req.params.id},function(err,opinion){
		if(err) return res.send(500, err.message);
		console.log('GET'+req.params.id+" : "+opinion)
		res.status(200).jsonp(opinion);
	})
});

router.get('/:id',function(req, res) {
	Opinion.findById(req.params.id, function(err, opinion) {
    if(err) return res.send(500, err.message);

    console.log('GET /opinion/' + req.params.id);
		res.status(200).jsonp(opinion);
	});
});

//POST - Insert a new TVShow in the DB
router.post('/',function(req, res) {
	console.log('POST');
	console.log(req.body);

	var opinion = new Opinion({
		puntuacion:    req.body.puntuacion,
		nivel: 	  req.body.nivel,
		comentario:	req.body.comentario,
		ruta:			req.body.ruta
	});

	opinion.save(function(err, opinion) {
		if(err) return res.send(500, err.message);
    res.status(200).jsonp(opinion);
	});
});


module.exports = router;
