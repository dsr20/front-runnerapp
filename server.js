var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    cors            = require("cors"),
    mongoose        = require('mongoose');

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cors());
app.use(express.static(__dirname+'/app'));

// LLAMADAS CONTROLLERS
var usuarios = require('./controllers/usuarios');
var opinion = require('./controllers/opinion');
var rutas = require('./controllers/rutas');

app.use('/api/usuarios',usuarios);
app.use('/api/opinion',opinion);
app.use('/api/rutas',rutas);

app.get('/', function(req, res) {
    console.log("GET /app index.html");
    res.sendFile(path.join(__dirname + '/app/index.html'));
});
/*var TVShowCtrl = require('./controllers/usuarios');

// Example Route
var router = express.Router();
router.get('/', function(req, res) {
  res.send("Hello world!");
});
app.use(router);

// API routes
var tvshows = express.Router();

tvshows.route('/tvshows')
  .get(TVShowCtrl.findAllTVShows)
  .post(TVShowCtrl.addTVShow);

tvshows.route('/tvshows/:id')
  .get(TVShowCtrl.findById)
  .put(TVShowCtrl.updateTVShow)
  .delete(TVShowCtrl.deleteTVShow);

app.use('/api', tvshows);*/
process.on('uncaughtException', function(err) {
    console.log(err);
});
// Connection to DB
mongoose.connect('mongodb://localhost/runner', function(err, res) {
  if(err) throw err;
  console.log('Connected to Database');
  // Start server
  app.listen(3000, function() {
    console.log("Node server running on http://localhost:3000");
  });
});
