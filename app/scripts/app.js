'use strict';

angular.module('frontend', [
    'ngStorage',
    'ngRoute',
    'ui.bootstrap',
    'ngCookies'

])
.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $routeProvider.
        when('/', {
            templateUrl: 'pages/home.html',
            controller: 'HomeCtrl'
        }).
        when('/rutas', {
            templateUrl: 'pages/rutas/rutas.html',
            controller: 'RutasCtrl'
        }).
        when('/rutas/:ruta_id',{
            templateUrl: 'pages/rutas/detalleruta.html',
            controller:   'HomeCtrl'
        }).
        when('/charts',{
            templateUrl: 'pages/charts/stadistics.html',
            controller:   'ChartsCtrl'
        }).
        when('/crearruta',{
            templateUrl: 'pages/rutas/newRoute.html',
            controller:   'crearRutaCtrl'
        }).
        when('/login',{
            templateUrl: 'pages/login.html',
            controller:   'MainCtrl'
        });

}]).run(['$window','auth','$rootScope', function($window,auth,$rootScope) { // CODIGO JQUERY QUE SE INICIA AL CARGAR EL DOCUI
  //al cambiar de rutas
    $rootScope.$on('$routeChangeStart', function() {
        //llamamos a checkStatus, el cual lo hemos definido en la factoria auth
        //la cuál hemos inyectado en la acción run de la aplicación
        auth.checkStatus();
    })
  $window.onload = function() {
       /* Sparklines can also take their values from the first argument
       passed to the sparkline() function */
       var myvalues = [10,8,5,7,4,4,1];

       /* The second argument gives options such as chart type */
       $('.sparkbar').sparkline(myvalues, {type: 'bar', barColor: 'green'} );
  };
}]);
