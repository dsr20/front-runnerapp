'use strict';

// Main es el nombre de la factoria que se encarga de hacer las peticiones al API
angular.module('frontend')
    .factory('Main', ['$http', '$localStorage', function($http, $localStorage){
        var baseUrl = "http://sca.ovh:443/api";
        return {
            save: function(data, success, error) {
                $http.post(baseUrl + '/signin', data).success(success).error(error)
            },
            login: function(username,password,data,error) {
                $http({
                    url: baseUrl + '/usuario/loginUsuario?email='+username+"&password="+password,
                    method: 'POST',
                    withCredentials: true,
                    transformResponse: [function (data) {
                        // Do whatever you want!
                        return data;
                    }]
                  }).success(data).error(error);
            },
            opiniones: function(success, error) {
                $http.get(baseUrl + '/opinion').success(success).error(error)
            },
            opiniones_id: function(id, success, error) {    // NOS DEVUELVE TODAS LAS OPINIONES RELACIONADAS A LA RUTA
                $http.get(baseUrl + '/opinion/devolverOpinionesRuta?idRuta=' + id).success(success).error(error)
            },
            rutas: function(success, error) {
                $http.get(baseUrl + '/ruta/devolverRutas').success(success).error(error)
            },
            rutas_id_sim: function(id, success, error) {
                $http.get(baseUrl + '/ruta/devolverRutaSimple?idRuta=' + id).success(success).error(error)
            },
            rutas_id_com: function(id, success, error) {
                $http.get(baseUrl + '/ruta/devolverRutaCompleta?idRuta=' + id).success(success).error(error)
            },
            rutas_del: function(id, success,error) {
                $http.delete(baseUrl + '/ruta/' + id).success(success).error(error)
            },
            datos_admin: function(username, password, success, err) {
                $http.post(baseUrl + '/usuario/dameUser?email='+username+"&password="+password,{withCredentials: true}).success(success).error(err)
            },
            usuario_realiza_ruta: function(success,error) {
                $http.get(baseUrl + '/ruta/dameUsuarioRealizaRuta',{withCredentials: true}).success(success).error(error);
            },
            dameTdsUsuarios: function(success,error){
                $http.post(baseUrl + '/usuario/dameUsuarios?email=admin@runmycity.es&password=admin',{withCredentials: true}).success(success).error(error);
            },
            estadisticas_sexo : function(sexo,fIni,fFin,success){
                $http.get(baseUrl + '/estadisticas/devolverEstadisticasSexo?sexo='+sexo+'&fechaInicio='+fIni +'&fechaFin='+fFin,{withCredentials: true}).success(success)
            },
            estadisticas_edad : function(sexo,fIni,fFin,success){
                $http.get(baseUrl + '/estadisticas/devolverEstadisticasSexo?sexo='+sexo+'&fechaInicio='+fIni +'&fechaFin='+fFin,{withCredentials: true}).success(success)
            },
            addRutas: function(data, success, error)
            {
              var str = baseUrl + '/ruta/crearRuta?email=' + data.email + "&password=" + data.pass + "&nombre=" +
                data.inputName + "&dificultad=" + data.inputLevel + "&descripcion=" + data.inputDesc + "&poblacion=" + data.inputPob + "&tipo=1";
                console.log(str);
              $http.post(str).success(success).error(error);
            },
            addPoints: function(dataP, success, error)
            {
              var str = baseUrl + '/punto/asignarPuntos?idRuta=' + dataP.ID + "&nombre=" + dataP.name + "&descripcion=" +
                dataP.desc + "&foto=" + dataP.url + "&coordx=" + dataP.lat + "&coordy=" + dataP.lng + "&email=" + dataP.email + "&password=" + dataP.pass;
                console.log(str);
              $http.post(str).success(success).error(error);
            }
        };
    }])
    .factory('pasaId', function() {
            var savedData = {}
            function set(data) {
              savedData = data;
            }
            function get() {
              return savedData;
            }
            return {
              set: set,
              get: get
            }
    })
    .factory('auth',['$cookies','$cookieStore','$location','$rootScope',function($cookies,$cookieStore,$location,$rootScope){
      return {
        login : function(username,password) {
            $rootScope.hideBar = true // LO USAMOS PARA VOLVER A PONER TODO EL MENU
            $cookies.username = username
            //$cookies.username = username
            $cookies.password = password
            $location.path("/")
        },
        logout : function() {
          $rootScope.hideBar = false
          $cookieStore.remove("username")
          $cookieStore.remove("password")
          $location.path("/login")
        },
        checkStatus : function() {
          //creamos un array con las rutas que queremos controlar
          var rutasPrivadas = ["/rutas","/charts","/"];
          if(this.in_array($location.path(),rutasPrivadas) && typeof($cookies.username) == "undefined") {
              $location.path("/login");
          }
          //en el caso de que intente acceder al login y ya haya iniciado sesión lo mandamos a la home
          if(this.in_array("/login",rutasPrivadas) && typeof($cookies.username) != "undefined") {
              $location.path("/");
          }
        },
        in_array : function(needle, haystack)
        {
            var key = '';
            for(key in haystack)
            {
                if(haystack[key] == needle)
                {
                    return true;
                }/*else if(haystack[key].indexOf("/rutas/")){
                    return true;
                }*/

            }
            return false;
        }

      }

    }]);
