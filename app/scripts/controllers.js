'use strict';

/* Controllers */
// Main es el nombre del servicio que nos ayuda a realiar las peticiones http al API
angular.module('frontend')
    .controller('HomeCtrl', ['$rootScope', // ------------------ CONTROLADOR HomeCtrl--------------------------------
    '$scope',
    '$location',
    '$localStorage',
    'Main',
    '$routeParams', function($rootScope, $scope, $location, $localStorage, Main, $routeParams) {

        //OBTENEMOS LOS DATOS DE LOS NUEVOS COMENTARIOS
        Main.usuario_realiza_ruta(function(res){
          $scope.rutasUsu =  res;
        },function(err){
          $rootScope.err = err;
        })

        // OBTENEMOS LOS DATOS PARA LOS CONTENEDORES
        Main.rutas(function(res){
          $scope.countRoutes = res.length
          cuentaPoblacion(res)
        },function(){})

        // OBTENEMOS LOS DATOS DE LOS NUEVOS CORREDORES
        Main.dameTdsUsuarios(function(res){
          $scope.countUsers = res.length;
          $scope.dataUsers = res;
          for(var i=0;i<res.length;i++){
            if(res[i].foto==undefined)
              res[i].foto = "dist/img/user1-128x128.jpg"
          }
        },function(){})

        function cuentaPoblacion(resultado){
          var res = resultado
          for(var i=0;i<res.length;i++) {
            for(var j=1;j<res.length;j++) {
              if(res[i].fk_poblacion == res[j].fk_poblacion)
                res.splice(j,1)
            }
          }
          $scope.countCities = res.length
        }
    }])
    .controller('RutasCtrl',['$rootScope',  // ------------------- CONTROLADOR RutasCtrl--------------------------------
    '$scope',
    '$location',
    '$localStorage',
    'Main',
    '$routeParams',
    'pasaId', function($rootScope, $scope, $location, $localStorage, Main, $routeParams,pasaId) {

          //pasaId.set($scope.rutaid);

          Main.rutas(function(resultado){
            $scope.rutas = resultado;
          }, function(){
              $rootScope.error = 'Failed to load resource';
          });

          $scope.deleteRuta = function(idRuta){
            Main.rutas_del(idRuta,function(res){
              $scope.rutas = res;
            })
          }

    }])
    .controller('RutasIdCtrl',['$rootScope',  // ------------------- CONTROLADOR RutasIdCtrl--------------------------------
    '$scope',
    '$location',
    '$localStorage',
    'Main',
    '$routeParams',
    'pasaId', function($rootScope, $scope, $location, $localStorage, Main, $routeParams,pasaId) {

          //var idRuta = pasaId.get();
          var items = [[38.383,-0.516094],[38.3835,-0.512264],[38.3844,-0.510668],[38.3855,-0.511156],[38.3866,-0.511526]];

          $scope.datos = items;

          Main.rutas_id_com($routeParams.ruta_id,function(resultado){
            $scope.ruta = resultado;
            Main.opiniones_id(resultado.id,function(res){
              $scope.opiniones = res
            },function(){})
          }, function(){
              $rootScope.error = 'Failed to load resource';
          });



    }])
    .controller('crearRutaCtrl',['$rootScope',  //------------------- CONTROLADOR crearRutaCtrl--------------------------------
    '$scope',
    '$location',
    '$localStorage',
    '$cookieStore',
    'Main',
    '$routeParams', function($rootScope, $scope, $location, $localStorage, $cookieStore, Main, $routeParams) {
      $scope.new = function()
      {
        var relleno = true;
        for(var i=0;i<$scope.points.length;i++)
        {
          if($scope.points[i].name=="" || $scope.points[i].desc=="")
          {
            relleno=false;
            break;
          }
        }
        console.log($cookieStore.get('username'));
        console.log($cookieStore.get('password'));
        if($scope.inputName != undefined && $scope.inputDesc != undefined && $scope.levelSelect != undefined && $scope.points.length != 0 && relleno==true)
        {
          var data = new Object();

          data.email = "admin@runmycity.es";
          data.pass = "admin";

          data.inputName = $scope.inputName;
          data.inputDesc = $scope.inputDesc;
          data.inputLevel = $scope.levelSelect;
          data.inputPob = $scope.inputPob;

          data.points = $scope.points;

          Main.addRutas(data,function(resultado)
            {
              for(var i=0;i<data.points.length;i++)
              {
                var dataP = new Object();

                dataP.ID = resultado;

                dataP.email = data.email;
                dataP.pass = data.pass;

                dataP.name = data.points[i].name;
                dataP.desc = data.points[i].desc;
                dataP.url = data.points[i].url;
                dataP.lat = data.points[i].lat;
                dataP.lng = data.points[i].lng;
                Main.addPoints(dataP,function(success){
                  if(i==data.points.length-1)
                  {
                    var divAlert = document.getElementById('info');
                    divAlert.setAttribute('style','margin-top:20px; display:;');
                    divAlert.setAttribute('class','alert alert-success');
                    divAlert.innerHTML="Ruta ["+data.inputName+"] creada con éxito";
                  }
                },function(error)
                  {
                    var divAlert = document.getElementById('info');
                    divAlert.setAttribute('style','margin-top:20px; display:;');
                    divAlert.setAttribute('class','alert alert-success');
                    divAlert.innerHTML="Ruta ["+data.inputName+"] creada con éxito";
                  });
              }
            },function(error){
              var divAlert = document.getElementById('info');
              divAlert.setAttribute('style','margin-top:20px; display:;');
              divAlert.setAttribute('class','alert alert-success');
              divAlert.innerHTML="Ruta ["+data.inputName+"] creada con éxito";
            });
        }
        else {
          var divAlert = document.getElementById('info');
          divAlert.setAttribute('style','margin-top:20px; display:;');
          divAlert.setAttribute('class','alert alert-danger');
          divAlert.innerHTML="<strong>ERROR: </strong> Campos sin completar.";
        }
      }
    }])
    .controller('ChartsCtrl',['$rootScope',   // ------------------- CONTROLADOR ChartsCtrl--------------------------------
    '$scope',
    '$location',
    '$localStorage',
    'Main',
    '$routeParams', function($rootScope, $scope, $location, $localStorage, Main, $routeParams) {
          Main.rutas(function(resultado){
            $scope.rutas = resultado;
          }, function(){
              $rootScope.error = 'Failed to load resource';
          });
    }])
    .controller('LoginCtrl',['$rootScope',   // ------------------- CONTROLADOR LoginCtrl (NO ESTA EN USO)--------------------------------
    '$scope',
    '$location',
    '$localStorage',
    'Main',
    '$routeParams',
    'auth', function($rootScope, $scope, $location, $localStorage, Main, $routeParams,auth) {

    }])
    .controller('MainCtrl',['$rootScope',   // ------------------- CONTROLADOR MainCtrl--------------------------------
    '$scope',
    '$location',
    '$localStorage',
    'Main',
    '$routeParams',
    'auth', function($rootScope, $scope, $location, $localStorage, Main, $routeParams,auth) {
        //la función login que llamamos en la vista llama a la función
        //login de la factoria auth pasando lo que contiene el campo
        //de texto del formulario
        //$rootScope.hideBar = true
        $scope.login = function()
        {
            Main.login($scope.username,$scope.password,function(success){
              console.log(success)
              // OBTENEMOS DATOS DEL USUARIO ADMINISTRADOR
              Main.datos_admin($scope.username,$scope.password,function(resultado){
                $rootScope.datos_admin = resultado;
              },function(err){
                $rootScope.err = err
              })
              // AÑADIMOS LAS COOKIES
              auth.login($scope.username,$scope.password)
            },function(error){
              $rootScope.err = error
            });

        }
        $scope.logout = function()
        {
            auth.logout()
        }
        $scope.Cambia = function(){
          if($rootScope.hideBar)
            $rootScope.hideBar = false
          else
            $rootScope.hideBar = true

        }

    }]);
