var mymap = L.map('mappu').setView([51.505, -0.09], 13);
var numMarks = 0;
angular.element(document.getElementById('crearRutaCtrlHTML')).scope().points=[];

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
  maxZoom: 18,
  attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
  id: 'mapbox.streets'
}).addTo(mymap);

mymap.panTo([38.3536, -0.48992]);

function onMapClick(e) {
    var aux = L.marker([e.latlng.lat, e.latlng.lng]).bindLabel(numMarks+1+"",{noHide: true,direction: 'right',className: 'sweet-deal-label',offset: [-13,-73]});
    aux.addTo(mymap)
      .bindPopup("<p style='text-align:center;'> Asocia una imagen <br /> a tu punto de referencia </p>");

    numMarks++

    var aux_p = angular.element(document.getElementById('crearRutaCtrlHTML')).scope().points;
    for(i=1;i<numMarks;i++)
    {
      aux_p[i-1].name = document.getElementById('inputNameMaker'+i).value;
      aux_p[i-1].desc = document.getElementById('inputDescMaker'+i).value;
    }

    var box = document.getElementById('MarkersBox');
    box.innerHTML += "<div class='box box-danger' id='divMarker"+numMarks+"'>"+
      "<div class='box-header with-border'>"+
        "<h3 class='box-title'>Punto de interés - <label id='labelId"+numMarks+"'>"+numMarks+"</label></h3>"+
        "<div class='box-tools pull-right'>"+
          "<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>"+
          "<button class='btn btn-box-tool' data-widget='remove' id='buttonEraseMarker"+numMarks+"' onclick='deleteMarker("+numMarks+")'><i class='fa fa-times'></i></button>"+
        "</div>"+
      "</div>"+
      "<div class='box-body'>"+
        "<img id='imgMarker"+numMarks+"' src='pages/rutas/marker.png' style='float:left;margin-right:32px;margin-left:16px;width:50px;height:82px;'></img>"+
        "<div style='display: -webkit-box;'>"+
          //"<label for='inputNameMaker"+numMarks+"' class='sr-only'>Nombre</label>"+
          "<input type='text' id='inputNameMaker"+numMarks+"' class='form-control' placeholder='Nombre' onchange='nameMarkerCH("+numMarks+")'/>"+
        "</div>"+
        "<div style='display: -webkit-box;'>"+
          //"<label for='inputDescMaker"+numMarks+"' class='sr-only'>Nombre</label>"+
          "<textarea row='2' type='text' id='inputDescMaker"+numMarks+"' class='form-control' placeholder='Descripción' onchange='textAreaMarkerCH("+numMarks+")'></textarea>"+
        "</div>"+
        "<div style='display: -webkit-box;'>"+
          "<button class='btn btn-danger' style='margin-right:20px;width:82px;' id='buttonNewImgMarker"+numMarks+"' onclick='newImage("+numMarks+")'>Examinar<br />imagen</button>"+
          "<div style='margin-top:5px;'>"+
            "<label id='latMaker"+numMarks+"'>Latitud: "+e.latlng.lat+"</label>"+
            "<br />"+
            "<label id='lngMaker"+numMarks+"'>Longitud: "+e.latlng.lng+"</label>"+
          "</div>"+
        "</div>"+
      "</div><!-- /.box-body -->"+
    "</div><!-- /.box -->";

    for(i=1;i<numMarks;i++)
    {
      document.getElementById('inputNameMaker'+i).value = aux_p[i-1].name;
      document.getElementById('inputDescMaker'+i).value = aux_p[i-1].desc;
    }

    var henga = new Object();
    henga.name = "";
    henga.desc = "";
    henga.lat = e.latlng.lat;
    henga.lng = e.latlng.lng;
    henga.mark = aux;
    angular.element(document.getElementById('crearRutaCtrlHTML')).scope().points[numMarks-1]=henga;
}

mymap.on('click', onMapClick);

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function deleteMarker(id)
{
  var aux_p = angular.element(document.getElementById('crearRutaCtrlHTML')).scope().points;
  document.getElementById("divMarker"+id).remove();
  mymap.removeLayer(aux_p[id-1].mark);
  aux_p.splice(id-1,1);
  for(i = id+1; i<=numMarks;i++)
  {
    aux_p[i-2].mark.label.setContent(i-1+"");

    document.getElementById("divMarker"+i).id = "divMarker"+(i-1);

    document.getElementById('labelId'+i).innerHTML = i-1;
    document.getElementById('labelId'+i).id = "labelId"+(i-1);

    document.getElementById('buttonEraseMarker'+i).setAttribute("onclick","deleteMarker("+(i-1)+")");
    document.getElementById('buttonEraseMarker'+i).id = "buttonEraseMarker"+(i-1);

    document.getElementById('imgMarker'+i).id = "imgMarker"+(i-1);

    document.getElementById('inputNameMaker'+i).setAttribute("onchange","nameMarkerCH("+(i-1)+")");
    document.getElementById('inputNameMaker'+i).id = "inputNameMaker"+(i-1);

    document.getElementById('inputDescMaker'+i).setAttribute("onchange","textAreaMarkerCH("+(i-1)+")");
    document.getElementById('inputDescMaker'+i).id = "inputDescMaker"+(i-1);

    document.getElementById('buttonNewImgMarker'+i).setAttribute("onclick","newImage("+(i-1)+")");
    document.getElementById('buttonNewImgMarker'+i).id = "buttonNewImgMarker"+(i-1);
  }
  numMarks--;
}

function newImage(id)
{
  var img = document.getElementById('imgMarker'+id);
  var str = prompt('Introduce la url de la imagen:');

  if(str==null || str=="")
  {
    str="pages/rutas/marker.png";
    img.setAttribute("style", "float:left;margin-right:32px;margin-left:16px;width:50px;height:82px;");
  }
  else {
    img.setAttribute("style", "float:left;margin-right:16px;margin-left:0px;width:82px;height:82px;");
  }
  angular.element(document.getElementById('crearRutaCtrlHTML')).scope().points[id-1].url=str;
  angular.element(document.getElementById('crearRutaCtrlHTML')).scope().$apply();
  img.src=str;
}

function nameMarkerCH(id)
{
  var txtA = document.getElementById('inputNameMaker'+id).value;
  angular.element(document.getElementById('crearRutaCtrlHTML')).scope().points[id-1].name=txtA;
  angular.element(document.getElementById('crearRutaCtrlHTML')).scope().$apply();
}

function textAreaMarkerCH(id)
{
  var txtA = document.getElementById('inputDescMaker'+id).value;
  angular.element(document.getElementById('crearRutaCtrlHTML')).scope().points[id-1].desc=txtA;
  angular.element(document.getElementById('crearRutaCtrlHTML')).scope().$apply();
}
